# bug-lamp2 - a weblog of random stuff.


Successfully deployed to the following URLs:

* [bug-lamp2-3tq7tq2tc.vercel.app](https://bug-lamp2-3tq7tq2tc.vercel.app)
* [bug-lamp2.vercel.app](http://bug-lamp2.vercel.app)
* [bug-lamp2.ret394.vercel.app](http://bug-lamp2.ret394.vercel.app)
* [bug-lamp2-git-master.ret394.vercel.app](http://bug-lamp2-git-master.ret394.vercel.app)


For something else -
  "Successfully" - as in the usual meaning when an institute says, we ensure every child's success. ;-)
